<?php

function getIsoArray($fileName){
    return json_decode(file_get_contents($fileName),true);
}

function getIso2to3($iso2){
    $isoJsonArray = getIsoArray('iso2ToIso3.json');
    $iso2 = strtoupper($iso2);
    if(array_key_exists($iso2,$isoJsonArray)){
        return $isoJsonArray[$iso2];
    }
    else{
        return $iso2;
    }
}

print getIso2to3('MX');